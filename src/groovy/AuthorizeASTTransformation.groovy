import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.AnnotationNode
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.expr.ArgumentListExpression
import org.codehaus.groovy.ast.expr.Expression
import org.codehaus.groovy.ast.expr.StaticMethodCallExpression
import org.codehaus.groovy.ast.stmt.BlockStatement
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation



/**
 * Created by sergey on 11/11/14.
 */

@GroovyASTTransformation(phase=CompilePhase.SEMANTIC_ANALYSIS)
public class AuthorizeASTTransformation implements ASTTransformation {

    @Override
    public void visit(ASTNode[] astNodes, SourceUnit sourceUnit) {
        if (astNodes != null) {
            for (ASTNode node : astNodes) {
                if (node instanceof MethodNode) {
                    MethodNode methodNode = (MethodNode) node;

                    List<AnnotationNode> annotations = methodNode.getAnnotations(new ClassNode(Authorize.class));

                    if (annotations != null && !annotations.isEmpty()) {
                        injectRolesCheck(methodNode, annotations)
                    }
                }
            }
        }
    }

    private void injectRolesCheck(MethodNode method, List<AnnotationNode> annotations) {
        for (AnnotationNode annotationNode : annotations) {
            BlockStatement code = (BlockStatement) method.getCode();

            Expression rolesValue = annotationNode.getMember("value");

            Expression checkRolesExpression = new StaticMethodCallExpression(
                    new ClassNode(Permissions.class),
                    "check",
                    new ArgumentListExpression(
                            rolesValue
                    )
            );

            code.getStatements().add(0, new ExpressionStatement(checkRolesExpression));
        }
    }
}
