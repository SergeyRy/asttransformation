/**
 * Created by sergey on 11/12/14.
 */
public enum Resourses {
    CREATE_PROJECT(1, "Create project"),
    EDIT_PROJECT(2, "Edit project"),
    CLOSE_PROJECT(3, "Close project"),
    DELETE_PROJECT(4, "Delete project");

    int id
    String name
}
