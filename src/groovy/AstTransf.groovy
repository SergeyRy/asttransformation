@Authorize(value = "Create project")
def createProject() {
    println "Project was created."
}

@Authorize(value = "Edit project")
def editProject() {
    println "Project was edited."
}

@Authorize(value = "Close project")
def closeProject() {
    println "Project was closed."
}

@Authorize(value = "Delete project")
def deleteProject() {
    println "Project was deleted."
}

createProject();
editProject();
closeProject();
deleteProject();