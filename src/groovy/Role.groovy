/**
 * Created by sergey on 11/11/14.
 */
public enum Role {
    ADMIN,
    SUPER_USER,
    USER
}