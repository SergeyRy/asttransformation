/**
 * Created by sergey on 11/11/14.
 */
class Permissions {
    static User getCurrentUser() {
        return new User(id: 1, name: "Sergey", role: Role.ADMIN)
    }


    public HashMap<Role, List<Resourses>> getPermissions() {
        HashMap<Role, List<Resourses>> rights = new HashMap<>()
        rights.put(Role.ADMIN, new ArrayList<Resourses>(Resourses.CREATE_PROJECT, Resourses.EDIT_PROJECT, Resourses.CLOSE_PROJECT, Resourses.DELETE_PROJECT))
        rights.put(Role.SUPER_USER, new ArrayList<Resourses>(Resourses.CREATE_PROJECT, Resourses.EDIT_PROJECT, Resourses.CLOSE_PROJECT))
        rights.put(Role.USER, new ArrayList<Resourses>(Resourses.CREATE_PROJECT, Resourses.EDIT_PROJECT))
    }
    static void check(String resoursName) {
        println(getCurrentUser().getName());
        println(resoursName);
    }
}
